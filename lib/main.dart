import 'package:flutter/material.dart';
import './models/transaction.dart';
import './widgets/new_ransaction.dart';
import './widgets/transaction_list.dart';
import './widgets/chart.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final List<Transaction> transactions = [
    Transaction(
      id: '4334',
      title: 'Sense of clorc',
      amount: 14.0,
      date: DateTime.now(),
    ),
    Transaction(
      id: '4335',
      title: 'Sense of Aliu',
      amount: 123.0,
      date: DateTime.now(),
    ),
    Transaction(
      id: '4336',
      title: 'Sense of Dean',
      amount: 5.0,
      date: DateTime.now(),
    ),
  ];
  bool traget = false;
  List<Transaction> get _recentTransactions {
    return transactions.where((tx) {
      return tx.date.isAfter(
        DateTime.now().subtract(
          Duration(days: 7),
        ),
      );
    }).toList();
  }

  _showForm(BuildContext ctx) {
    showModalBottomSheet(
        context: ctx,
        builder: (bctx) {
          return Container(
            // height: MediaQuery.of(context).size.height * 0.2,
            child: NewTransaction(_addTransaction),
          );
        });
  }

  _addTransaction(String title, double amount, DateTime date) {
    final trans = Transaction(
      title: title,
      amount: amount,
      id: DateTime.now().toString(),
      date: date,
    );

    setState(() {
      transactions.add(trans);
    });
  }

  void _deleteTransaction(String id) {
    setState(() {
      transactions.removeWhere((tx) => tx.id == id);
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
          textTheme: ThemeData.light().textTheme.copyWith(
                title: TextStyle(color: Theme.of(context).primaryColor),
              ),
          appBarTheme: AppBarTheme(
            textTheme: ThemeData.light()
                .textTheme
                .copyWith(title: TextStyle(fontSize: 20)),
          )),
      home: Scaffold(
        backgroundColor: Theme.of(context).primaryColor,
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.transparent,
          actions: [
            IconButton(
              icon: Icon(Icons.add),
              onPressed: () {
                _showForm(context);
              },
            ),
          ],
          title: Text('App'),
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(traget ? Icons.check : Icons.add),
          onPressed: () {
            setState(() {
              traget = !traget;
            });
          },
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Chart(_recentTransactions),
              traget ? NewTransaction(_addTransaction) : Center(),
              TransactionList(transactions, _deleteTransaction),
            ],
          ),
        ),
      ),
    );
  }
}
