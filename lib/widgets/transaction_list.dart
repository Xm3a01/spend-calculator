import 'package:flutter/material.dart';
import 'package:my_app/helpers/DateConvert.dart';
import '../models/transaction.dart';

class TransactionList extends StatefulWidget {
  final Function deleteTransaction;
  final List<Transaction> userTransactions;
  TransactionList(this.userTransactions, this.deleteTransaction);

  @override
  _TransactionListState createState() => _TransactionListState();
}

class _TransactionListState extends State<TransactionList> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.6,
      child: widget.userTransactions.isEmpty
          ? Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Text(
                  'No Transactions yet !',
                  style: TextStyle(
                    color: Colors.grey[200],
                    fontSize: 20,
                  ),
                ),
                Image.asset(
                  'assets/images/waiting.png',
                  scale: 3,
                ),
              ],
            )
          : ListView.builder(
              itemBuilder: (cnxt, index) {
                return Card(
                  elevation: 6,
                  // margin: EdgeInsets.symmetric(vertical: 8, horizontal: 8),
                  child: ListTile(
                    leading: CircleAvatar(
                      child: Padding(
                        padding: const EdgeInsets.all(10),
                        child: FittedBox(
                          child: Text(
                            '\$${widget.userTransactions[index].amount}',
                          ),
                        ),
                      ),
                      radius: 30,
                    ),
                    title: Text(widget.userTransactions[index].title),
                    subtitle: Text(
                      DateConvert.ddMMyy(widget.userTransactions[index].date),
                    ),
                    trailing: IconButton(
                      icon: Icon(
                        Icons.delete,
                        color: Colors.red,
                      ),
                      onPressed: () {
                        widget.deleteTransaction(
                            widget.userTransactions[index].id);
                      },
                    ),
                  ),
                );
              },
              itemCount: widget.userTransactions.length,
            ),
    );
  }
}
