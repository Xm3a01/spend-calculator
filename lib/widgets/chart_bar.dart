import 'package:flutter/material.dart';
import 'package:my_app/helpers/DateConvert.dart';

class ChartBar extends StatelessWidget {
  final int dayOfWeek;
  final double spedngAmonut;
  final double spendingPcTotal;

  ChartBar(this.dayOfWeek, this.spedngAmonut, this.spendingPcTotal);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text('\$${spedngAmonut.toStringAsFixed(0)}'),
        SizedBox(height: 4),
        Container(
          height: 100,
          width: 10,
          child: Stack(
            children: [
              Container(
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.grey, width: 1.0),
                  color: Color.fromRGBO(220, 220, 220, 1),
                  borderRadius: BorderRadius.circular(20),
                ),
              ),
              FractionallySizedBox(
                heightFactor: spendingPcTotal,
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.blue[200],
                    borderRadius: BorderRadius.circular(20),
                  ),
                ),
              ),
            ],
          ),
        ),
        SizedBox(height: 4),
        Text(
          DateConvert.weekDay(dayOfWeek),
        )
      ],
    );
  }
}
