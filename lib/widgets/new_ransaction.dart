import 'package:flutter/material.dart';
import '../helpers/DateConvert.dart';

class NewTransaction extends StatefulWidget {
  final Function addTx;

  NewTransaction(this.addTx);

  @override
  _NewTransactionState createState() => _NewTransactionState();
}

class _NewTransactionState extends State<NewTransaction> {
  final titleContraller = TextEditingController();

  final amountContraller = TextEditingController();

  DateTime _selectedDate;

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height * 0.2,
        padding: EdgeInsets.symmetric(horizontal: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Column(
              children: [
                Container(
                  margin: EdgeInsets.all(10),
                  child: Text(
                    'Add Form',
                    style: TextStyle(
                      color: Colors.grey[700],
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ],
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                TextField(
                  decoration: InputDecoration(labelText: 'Title'),
                  controller: titleContraller,
                ),
                TextField(
                  decoration: InputDecoration(labelText: 'Amount'),
                  controller: amountContraller,
                ),
                Container(
                  height: 70,
                  child: Row(
                    // mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Expanded(
                        child: Text(
                          _selectedDate == null
                              ? 'No Date chosen !'
                              : 'Date selected: ' +
                                  DateConvert.ddmmyy(_selectedDate),
                          style: TextStyle(fontSize: 16),
                        ),
                      ),
                      IconButton(
                        icon: Icon(Icons.date_range),
                        onPressed: () => {
                          showDatePicker(
                            context: context,
                            initialDate: DateTime.now(),
                            firstDate: DateTime(2005),
                            lastDate: DateTime.now(),
                          ).then((pickeDate) {
                            if (pickeDate == null) {
                              return;
                            }
                            setState(() {
                              _selectedDate = pickeDate;
                            });
                          })
                        },
                      ),
                      // TextField()
                    ],
                  ),
                ),
                FlatButton(
                  onPressed: () {
                    if (_selectedDate == null) {
                      return;
                    }
                    widget.addTx(
                      titleContraller.text,
                      double.parse(amountContraller.text),
                      _selectedDate,
                    );
                  },
                  child: Text(
                    'Add transaction',
                    style: const TextStyle(
                      color: Colors.purple,
                    ),
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
