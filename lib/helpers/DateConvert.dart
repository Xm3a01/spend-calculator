class DateConvert {
  static List<String> weekday = ['Mo', 'Tu', 'Win', 'Th', 'Fr', 'Sa', 'Su'];
  static List<String> yearmonth = [
    'Jnu',
    'Fur',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dec'
  ];

  static String ddmmyy(DateTime date) {
    return '${date.day}/${date.month}/${date.year}';
  }

  static String weekDay(int index) {
    return weekday[index - 1];
  }

  static String yearMonth(int month) {
    return yearmonth[month - 1];
  }

  static String DDMMyy(DateTime date) {
    return weekday[date.weekday - 1] +
        '/' +
        yearmonth[date.month - 1] +
        '/' +
        '${date.year}';
  }

  static String ddMMyy(DateTime date) {
    return yearmonth[date.month - 1] + ' ${date.day},${date.year}';
  }
}
